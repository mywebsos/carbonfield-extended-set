/**
 * External dependencies.
 */
import SetField from "../vendor/htmlburger/carbon-fields/packages/core/fields/set";

class ExtendedSetField extends SetField {}

export default ExtendedSetField;
