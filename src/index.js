/**
 * External dependencies.
 */
import { registerFieldType } from "@carbon-fields/core";

/**
 * Internal dependencies.
 */
import "./style.scss";
import ExtendedSetField from "./main";

registerFieldType("extended_set", ExtendedSetField);
