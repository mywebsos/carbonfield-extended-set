<?php

use Carbon_Fields\Carbon_Fields;
use Carbon_Field_Extended_Set\Extended_Set_Field;

define( 'Carbon_Field_Extended_Set\\DIR', __DIR__ );

Carbon_Fields::extend( Extended_Set_Field::class, function( $container ) {
	return new Extended_Set_Field(
		$container['arguments']['type'],
		$container['arguments']['name'],
		$container['arguments']['label']
	);
} );
